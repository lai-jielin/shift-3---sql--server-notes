
--练习 group by
--连接(多表) 查询
--嵌套查询(子查询):关联子查询
--分页查询
--E-R图
--三范式(第一范式，第二范式，第三范式)
--2节课




if exists (select * from sys.databases where name = 'TEST01')
drop database TEST01
go

create database TEST01
go

use TEST01
go

create table StudentInfo(
	stuNo varchar(10) primary key,
	stuName nvarchar(10) not null,
	stuAge tinyint not null,
	stuAddress nvarchar(10) not null,
	stuSeat tinyint not null,
	stuSex bit not null check(stuSex in (0,1) )
)

insert StudentInfo values
('S2501','张秋利',20,'美国硅谷',1,1),
('S2502','李斯文',18,'湖北武汉',2,0),
('S2503','马文才',22,'湖南长沙',3,0),
('S2504','欧阳俊雄',21,'湖北武汉',4,0),
('S2505','梅超风',20,'湖北武汉',5,1),
('S2506','陈旋风',19,'美国硅谷',6,1),
('S2507','陈凤',20,'美国硅谷',7,0);

create table ExamInfo(
	examNo int primary key identity(1,1),
	stuNo varchar(10) references StudentInfo(StuNo),
	wittenExam tinyint not null,
	labExam tinyint not null
);

insert ExamInfo values
('S2501',50,70),
('S2502',60,65),
('S2503',86,85),
('S2504',40,80),
('S2505',70,90),
('S2506',85,90);


--修改表: 删除约束:alter table 表名 drop contraint 约束名
alter table StudentInfo drop constraint CK__StudentIn__stuSe__108B795B
