create database Student

use Student

create table StudentInfo(
    stuNo int primary key identity(2501,1),
	stuName varchar(10) not null,
	stuAge int,
	stuAddress nvarchar(20),
	stuSeat int,
	stuSex char(2) check(stuSex='1' or stuSex='0'),
 )
insert into StudentInfo(stuName,stuAge,stuAddress,stuSeat,stuSex)
values('张秋利','20','美国硅谷','1','1'),
      ('李斯文','18','湖北武汉','2','0'),
      ('马文才','22','湖南长沙','3','1'),
      ('欧阳俊雄','21','湖北武汉','4','0'),
	  ('梅超风',20,'湖北武汉','5','1'),
	  ('陈旋风',19,'美国硅谷','6','1'),
	  ('陈风',20,'美国硅谷','7','0');

create table ExamInfo(
    examNo int primary key identity(1,1),
	stuNo int foreign key references StudentInfo(stuNo),
	writtenExam int ,
	LabExam int,
   )

insert ExamInfo(stuNo,writtenExam,labExam)
 values(2501,50,70),
	   (2502,60,65),
	   (2503,86,85),
	   (2504,40,80),
	   (2505,70,80),
	   (2506,85,90);

--1.查询学生信息表（stuinfo）中所有列信息，给每列取上中文名称
select stuNo'编号',stuName'姓名',stuAge'年龄',stuAddress'住址',stuSeat'座位',stuSex'性别' from StudentInfo

--2.查询学生信息表（stuinfo）中的姓名，年龄和地址三列的信息
select stuName,stuAge,stuAddress from StudentInfo

--3.查询学生分数表（stuexam）中的学号，笔试和机试三列的信息，并为这三列取中文名字
select examNo'学号',writtenExam'笔试',LabExam'机试' from ExamInfo
--select examNo 学号,writtenExam 笔试,LabExam 机试 from ExamInfo

--4.查询学生信息表（stuInfo）中的学号，姓名，地址，以及将：姓名+@+地址 组成新列 “邮箱”
select stuNo,stuName,stuAddress from StudentInfo

alter table StudentInfo add StuEmail nvarchar(50)
update StudentInfo set stuEmail ='stuName@stuaddress' where stuNo='2501'

--5.查询学生分数表（stuexam）中的学生的学号，笔试，机试以及总分这四列的信息
select stuNo,writtenExam,LabExam,(writtenExam+LabExam)总分 from ExamInfo 


--6.查询学生信息表（stuInfo）中学生来自哪几个地方 
select distinct stuAddress from StudentInfo

--7.查询学生信息表（stuInfo）中学生有哪几种年龄，并为该列取对应的中文列名
select distinct stuAge'年龄' from StudentInfo

--8.查询学生信息表（stuInfo）中前3行记录 
select top 3 * from StudentInfo

--
--9.查询学生信息表（stuInfo）中前4个学生的姓名和座位号
select top 4 stuName,stuSeat from StudentInfo 

--10.查询学生信息表（stuInfo）中一半学生的信息
select top 50 percent * from StudentInfo

--11.将地址是湖北武汉，年龄是20的学生的所有信息查询出来
select * from StudentInfo
where stuAddress='湖北武汉'and stuAge=20

--12.将机试成绩在60-80之间的信息查询出来，并按照机试成绩降序排列（用两种方法实现）
select * from ExamInfo 
where LabExam between 60 and 80 order by LabExam  desc
--where LabExam >= 60 and LabExam <=80  order by LabExam  desc

--13.查询来自湖北武汉或者湖南长沙的学生的所有信息（用两种方法实现）
select * from StudentInfo
where stuAddress ='湖北武汉' or stuAddress = '湖南长沙'
--where stuAddress in('湖北武汉','湖南长沙')

--14.查询出笔试成绩不在70-90之间的信息,并按照笔试成绩升序排列（用两种方法实现）
select * from ExamInfo 
where writtenExam  not  between 70 and 90 order by writtenExam  desc
--where writtenexam <70 or writtenexam >90

--15.查询年龄没有写的学生所有信息
select * from StudentInfo
where stuAge is null

--16.查询年龄写了的学生所有信息
select * from StudentInfo
where stuAge is not null

--17.查询姓张的学生信息
select * from StudentInfo
where stuName like '张%'

--18.查询学生地址中有‘湖’字的信息
select * from StudentInfo
where stuAddress like '%湖%'

--19.查询姓张但名为一个字的学生信息
select * from StudentInfo
where stuName like '张_'

--20.查询姓名中第三个字为‘俊’的学生的信息，‘俊’后面有多少个字不限制
select * from StudentInfo
where stuName like '__俊%'

--21.按学生的年龄降序显示所有学生信息
select *from StudentInfo 
order by stuAge desc

--22.按学生的年龄降序和座位号升序来显示所有学生的信息
select * from StudentInfo 
order by stuAge desc,stuSeat 


--23显示笔试第一名的学生的考试号，学号，笔试成绩和机试成绩
select top 1 stuNo 学号,examNo 考试号,writtenExam 笔试成绩,labexam 机试成绩,max(writtenExam)最高分 from ExamInfo
group by stuNo,examNo,writtenExam,LabExam order by writtenExam desc

--24.显示机试倒数第一名的学生的考试号，学号，笔试成绩和机试成绩
select top 1 stuNo 学号,examNo 考试号,writtenExam 笔试成绩,labexam 机试成绩,min(labexam)最低分 from ExamInfo
group by stuNo,examNo,writtenExam,LabExam order by LabExam


--25.查询每个地方的学生的平均年龄
select stuaddress 地方,avg(StuAge) from StudentInfo
group by stuAddress

--26.查询男女生的分别的年龄总和
select stusex 性别,sum(stuAge)年龄总和 from StudentInfo
group by stusex


--27.查询每个地方的男女生的平均年龄和年龄的总和
select stuaddress 地方,stuSex 性别,avg(StuAge)平均年龄 ,sum(stuAge)年龄总和 from StudentInfo
group by stuAddress,stuSex





